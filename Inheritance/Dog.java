// Child class inheriting from Animal
public class Dog extends Animal {
    // Method in the child class
    public void bark() {
        System.out.println("The dog barks.");
    }

public static void main(String[] args) {
        // Create an instance of the Dog class
        Dog myDog = new Dog();

        // Call methods from both the parent class and the child class
        myDog.eat(); // Method from the Animal class
        myDog.bark(); // Method from the Dog class
        myDog.wavingtail();
    }
}
