package Javaclass;
import java.util.Arrays;
import java.util.Scanner;

public class DescendingOrder {
	public static void main(String[] args) {

	
	
	        Scanner sc = new Scanner(System.in);
	        System.out.print("Enter the size of the array:");
	        int size = sc.nextInt();
	        int[] n = new int[size];
	        for(int i = 0; i < n.length; i++) {
	            System.out.print("Enter the value: ");
	            n[i] = sc.nextInt();
	        }
	        System.out.println("Before Sorting: " + Arrays.toString(n));
            Arrays.sort(n);
            for(int i=0;i<n.length/2;i++)
            {
            	int temp = n[i];
            	n[i]=n[n.length-1-i];	
            	n[n.length-1-i] = temp;
            }
	        System.out.println("After Sorting in Descending Order: " + Arrays.toString(n));
	    }
	}


