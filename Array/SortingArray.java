package Javaclass;
import java.util.Arrays;
import java.util.Scanner;
public class SortingArray {
	public static void main(String[] args) {

     // int[] n = {5, 2, 9, 1, 5, 6};
	        Scanner sc = new Scanner(System.in);
	        System.out.print("Enter the size of the array:");
	        int size= sc.nextInt();
	        int[] n= new int[size];
	        for(int i=0;i<n.length;i++)
	        {
	        	System.out.print("Enter the values");
	        	n[i]=sc.nextInt();
	        }
	        System.out.println("Before Sorting:" + Arrays.toString(n));
	        Arrays.sort(n);
	        System.out.println("After Sorting:" + Arrays.toString(n));

		
	}

}
