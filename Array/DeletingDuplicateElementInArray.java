package Javaclass;
import java.util.Scanner;

public class DeletingDuplicateElementInArray {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of the array:");
        int size = sc.nextInt();
        int[] n = new int[size];

        for (int i = 0; i < n.length; i++) {
            System.out.print("Enter the value for element " + (i + 1) + ": ");
            n[i] = sc.nextInt();
        }

        int[] uniqueArray = new int[size];
        int uniqueIndex = 0;

        for (int j = 0; j < n.length; j++) {
            int num = n[j];
            if (num == -1) continue; 

            boolean isDuplicate = false;
            for (int i = j + 1; i < n.length; i++) {
                if (n[i] == num) {
                    n[i] = -1; 
                    isDuplicate = true;
                }
            }

            if (!isDuplicate) {
                uniqueArray[uniqueIndex++] = num; 
            }
        }

       
        System.out.println("Array after removing duplicates:");
        for (int i = 0; i < uniqueIndex; i++) {
            System.out.print(uniqueArray[i] + " ");
        }
    }
}
