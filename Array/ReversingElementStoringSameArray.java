package Javaclass;

public class ReversingElementStoringSameArray {

	public static void main(String[] args) {
		//Reversing the elements and storing in Same Array
		int[] a = {5,10,15,20};
		int i = 0, j=a.length-1;
		while(i<j) 
		{
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp; 
		i++; j--;
		}


		for(i = 0; i<a.length;i++)
		  System.out.print(a[i]+ " ");
	}

}
