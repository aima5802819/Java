package Javaclass;
import java.util.Scanner;

public class InsertElementInArray {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the Size of the array:");
        int size=sc.nextInt();
        int[] n= new int[size+1];
        for(int i=0;i<size;i++)
        {
        	System.out.println("Enter the  values of array:");
        	n[i]=sc.nextInt();
        }
        System.out.print("Enter the insert element of array:");
        int insertElemnt=sc.nextInt();
      
        System.out.print("Enter the position to insert the element (1 to " + (size+1)+ "): ");
        int position = sc.nextInt();
        
        // Validate position
        if (position < 1 || position > size + 1) {
            System.out.println("Invalid position. Please enter a position between 1 and " + (size + 1) + ".");
            return;
        }

        // Insert the element by shifting elements to the right
        for (int i = size; i >= position; i--) {
            n[i] = n[i - 1];
        }
        n[position - 1] = insertElemnt;

        // Print the array after insertion
        System.out.println("Array after insertion:");
        for (int i = 0; i <= size; i++) {
            System.out.print(n[i] + " ");

}
	}
}

