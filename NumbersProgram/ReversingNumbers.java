class ReversingNumbers {
    public static void main(String[] args) {
        int n = 5482;
        int n1 = n;
        int rev = 0;

        while (n != 0) {
            int rem = n % 10;
            rev = rev * 10 + rem;
            n = n / 10;
        }

        System.out.println("Reversed number: " + rev);
}
}
