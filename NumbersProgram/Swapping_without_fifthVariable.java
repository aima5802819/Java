package Javaclass;

public class Swapping_without_fifthVariable {

	public static void main(String[] args) {
		int a=5,b=6,c=7,d=8;
		a=a+b+c+d;
		b=a-(b+c+d);
		c=a-(b+c+d);
		d=a-(b+c+d);
		a=a-(b+c+d);
		System.out.println("After Swapping");
		System.out.println("a = " + a);
		System.out.println("b = " + b);
		System.out.println("c = " + c);	
		System.out.println("d = " + d);	

	}

}
